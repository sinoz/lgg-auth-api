import * as authApi from "@latency.gg/lgg-auth-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import * as operations from "../operations/index.js";
import { Context } from "./context.js";

export type Server = authApi.Server;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new authApi.Server({});

    server.registerMiddleware(oas3ts.createErrorMiddleware(onError));

    server.registerMiddleware(
        async function (this, route, request, next) {
            const operation = route ?
                oas3ts.getOperationId(
                    authApi.metadata,
                    route.name,
                    request.method,
                ) :
                undefined;

            const stopTimer = context.metrics.operationDuration.startTimer({ operation });
            try {
                const response = await next(request);
                stopTimer({ status: response.status });
                return response;
            }
            catch (error) {
                stopTimer();

                throw error;
            }
        },
    );

    server.registerCreateClientOperation(
        operations.createCreateClientOperation(context),
    );
    server.registerSubscribeClientEventsOperation(
        operations.createSubscribeClientEventsOperation(context),
    );

    server.registerCreateClientSecretOperation(
        operations.createCreateClientSecretOperation(context),
    );
    server.registerValidateClientSecret2Operation(
        operations.createValidateClientSecret2Operation(context),
    );

    server.registerCreateRedirectUriOperation(
        operations.createCreateRedirectUriOperation(context),
    );
    server.registerValidateRedirectUriOperation(
        operations.createValidateRedirectUriOperation(context),
    );

    server.registerCreateUserOperation(
        operations.createCreateUserOperation(context),
    );

    server.registerConnectUserToProviderSubjectOperation(
        operations.createConnectUserToProviderSubjectOperation(context),
    );
    server.registerDisconnectUserFromProviderSubjectOperation(
        operations.createDisconnectUserFromProviderSubjectOperation(context),
    );

    server.registerCreateAuthorizationCodeOperation(
        operations.createAuthorizationCodeOperation(context),
    );
    server.registerCreateAccessTokenOperation(
        operations.createCreateAccessTokenOperation(context),
    );
    server.registerCreateRefreshTokenOperation(
        operations.createCreateRefreshTokenOperation(context),
    );

    server.registerResolveUserFromAuthorizationCodeOperation(
        operations.createResolveUserFromAuthorizationCodeOperation(context),
    );
    server.registerResolveUserFromProviderSubjectOperation(
        operations.createResolveUserFromProviderSubjectOperation(context),
    );

    //#region deprecated

    server.registerValidateClientSecretOperation(
        operations.createValidateClientSecretOperation(context),
    );

    //#endregion

    return server;
}
