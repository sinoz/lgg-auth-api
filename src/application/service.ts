import delay from "delay";
import pg from "pg";
import * as queries from "../queries/index.js";
import { Config } from "./config.js";

export interface Services {
    pgPool: pg.Pool;

    clientRowQuery: queries.ClientRowQuery,

    clientQuery: queries.ClientQuery,

    destroy: () => Promise<void>
}

export function createServices(
    config: Config,
    onError: (error: unknown) => void,
): Services {
    const pgPool = new pg.Pool({
        connectionString: config.pgUri.toString(),
    });

    const clientRowQuery = queries.createClientRowQueryFactory(
        config,
        { pgPool },
        onError,
    );

    const clientQuery = queries.createClientQueryFactory(
        config,
        { clientRowQuery },
        onError,
    );

    const destroy = async () => {
        // flushing should happen after all of the leases are released!
        // we wait for a tick to make this happen
        await delay(0);
        //

        clientQuery.flush();
        clientRowQuery.flush();

        await pgPool.end();
    };

    return {
        pgPool,
        clientRowQuery,
        clientQuery,

        destroy,
    };
}
