export function isErrorEvent(event: ErrorEvent | object): event is ErrorEvent {
    return "error" in event && event.error;
}

export interface ErrorEvent {
    error: true;
}
