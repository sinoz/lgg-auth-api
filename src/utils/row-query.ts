import assert from "assert";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as pg from "pg";
import { RowFilter } from "table-access";
import { ErrorEvent, isErrorEvent } from "./error.js";
import { createQueryFactory, QuerySource } from "./query.js";

export interface TableSnapshotEvent<Row extends object> {
    type: "snapshot",
    rows: Row[]
}
export interface TableInsertRowEvent<Key extends object, Data extends object> {
    type: "insert",
    key: Key,
    data: Data,
}
export interface TableUpdateRowEvent<Key extends object, Data extends object> {
    type: "update",
    key: Key,
    data: Data,
}
export interface TableDeleteRowEvent<Key extends object> {
    type: "delete",
    key: Key,
}

export type TableEventUnion<Key extends object, Data extends object> =
    TableSnapshotEvent<Key & Data> |
    TableInsertRowEvent<Key, Data> |
    TableUpdateRowEvent<Key, Data> |
    TableDeleteRowEvent<Key>;

export interface RowQueryOptions<Key extends object, Data extends object> {
    pool: pg.Pool,
    getPrimaryKey: (key: Key) => string,
    getIndexKey: Record<string, (row: Key & Data) => string>,
    onError: (error: unknown) => void
    retryIntervalBase: number, retryIntervalCap: number,
    createTableEvents(
        pool: pg.Pool,
        filter: Partial<Key> | RowFilter<Key>,
        signal: AbortSignal,
    ): AsyncIterable<TableEventUnion<Key, Data> | ErrorEvent>,
}

export type RowQueryState<Row extends object, Indices extends string = string> = {
    rows: immutable.Map<string, Row>
    indices: Record<Indices, immutable.Map<string, immutable.Set<string>>>
    error: boolean
}

export type RowQueryFactory<Key extends object, Data extends object> =
    InstanceMemoizer<Promise<RowQueryInstance<Key, Data>>, [Partial<Key>]>;

export type RowQueryInstance<Key extends object, Data extends object> =
    QuerySource<RowQueryState<Key & Data>, TableEventUnion<Key, Data> | ErrorEvent>

export function createRowQueryFactory<
    Key extends object, Data extends object
>(
    options: RowQueryOptions<Key, Data>,
): RowQueryFactory<Key, Data> {
    type Row = Key & Data;
    type State = RowQueryState<Row>;
    type Event = TableEventUnion<Key, Data> | ErrorEvent;

    const {
        pool,
        getPrimaryKey,
        getIndexKey,
        onError,
        retryIntervalBase, retryIntervalCap,
        createTableEvents,
    } = options;

    const indices = Object.keys(getIndexKey);

    const initialState: State = {
        rows: immutable.Map(),
        indices: {},
        error: false,
    };
    const reduce = (state: State, event: Event) => {
        if (isErrorEvent(event)) {
            return {
                ...state,
                error: true,
            };
        }

        switch (event.type) {
            case "snapshot": {
                const { rows } = event;

                const state: State = {
                    rows: immutable.Map(),
                    indices: Object.fromEntries(
                        indices.map(index => [index, immutable.Map()]),
                    ),
                    error: false,
                };

                state.rows = state.rows.asMutable();
                for (const index of indices) {
                    state.indices[index] = state.indices[index].asMutable();
                }

                for (const row of rows) {
                    const primaryKey = getPrimaryKey(row);
                    state.rows.set(primaryKey, row);

                    for (const index of indices) {
                        const indexKey = getIndexKey[index](row);
                        state.indices[index].update(
                            indexKey,
                            primaryKeys => (primaryKeys ?? immutable.Set()).add(primaryKey),
                        );
                    }
                }

                state.rows = state.rows.asImmutable();
                for (const index of indices) {
                    state.indices[index] = state.indices[index].asImmutable();
                }

                return state;
            }

            case "insert": {
                const { key, data } = event;
                const row = { ...key, ...data };

                const primaryKey = getPrimaryKey(key);

                assert(!state.rows.has(primaryKey), "expected no row");

                state = {
                    ...state,
                    rows: state.rows.set(
                        primaryKey, row,
                    ),
                    indices: Object.fromEntries(indices.map(index => {
                        const indexKey = getIndexKey[index](row);
                        return [
                            index,
                            state.indices[index].update(
                                indexKey,
                                primaryKeys => (primaryKeys ?? immutable.Set()).add(primaryKey),
                            ),
                        ];
                    })),
                };

                return state;
            }

            case "update": {
                const { key, data } = event;
                const row = { ...key, ...data };

                const primaryKey = getPrimaryKey(key);
                const rowPrevious = state.rows.get(primaryKey);

                assert(rowPrevious != null, "expected row");

                state = {
                    ...state,
                    rows: state.rows.set(
                        primaryKey, row,
                    ),
                    indices: Object.fromEntries(indices.map(index => {
                        const indexKey = getIndexKey[index](row);
                        const indexKeyPrevious = getIndexKey[index](rowPrevious);

                        return [
                            index,
                            state.indices[index].
                                update(
                                    indexKeyPrevious,
                                    primaryKeys => {
                                        assert(primaryKeys != null, "expected index entry");
                                        return primaryKeys.remove(indexKeyPrevious);
                                    },
                                ).
                                update(
                                    indexKey,
                                    primaryKeys => (primaryKeys ?? immutable.Set()).add(primaryKey),
                                ),
                        ];
                    })),
                };

                return state;
            }

            case "delete": {
                const { key } = event;

                const primaryKey = getPrimaryKey(key);
                const rowPrevious = state.rows.get(primaryKey);

                assert(rowPrevious != null, "expected row");

                state = {
                    ...state,
                    rows: state.rows.remove(
                        primaryKey,
                    ),
                    indices: Object.fromEntries(indices.map(index => {
                        const indexKeyPrevious = getIndexKey[index](rowPrevious);

                        return [
                            index,
                            state.indices[index].
                                update(
                                    indexKeyPrevious,
                                    primaryKeys => {
                                        assert(primaryKeys != null, "expected index entry");
                                        return primaryKeys.remove(indexKeyPrevious);
                                    },
                                ),
                        ];
                    })),
                };

                return state;
            }

        }
    };
    const settle = (state: State, event: Event) => true;

    async function* createSource(signal: AbortSignal, filter: Partial<Key>) {
        try {
            yield* createTableEvents(
                pool,
                filter,
                signal,
            );
        }
        catch (error) {
            if (!signal.aborted) {
                yield { error: true } as const;
                throw error;
            }
        }
    }
    const calculateHash = (filter: Partial<Key>) => JSON.stringify(filter);

    const factory = createQueryFactory({
        initialState, reduce, settle,
        createSource,
        calculateHash,
        onError,
        retryIntervalBase, retryIntervalCap,
    });
    return factory;
}
