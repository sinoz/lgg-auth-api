import { assertAbortSignal, setupCascadingAbort } from "abort-tools";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import { createBufferedAsyncIterable } from "query-source";
import { QuerySource, QuerySourceElement } from "./index.js";

export interface LeadFollowEventsConfig<
    E, FollowKey extends string | number,
    LeadState, LeadEvent, LeadArgs extends Array<unknown>,
    FollowState, FollowEvent, FollowArgs extends Array<unknown>,
    > {
    /*
    we need a signal here to abort this query.
    the lead-follow query is responsible to create both the lead and follow queries. Therefore
    it will also need a signal to abort the lead (and follow) queries.
    */
    signal: AbortSignal,

    leadQueryFactory: InstanceMemoizer<
        Promise<QuerySource<LeadState, LeadEvent>>,
        LeadArgs
    >;
    followQueryFactory: InstanceMemoizer<
        Promise<QuerySource<FollowState, FollowEvent>>,
        FollowArgs
    >;

    getFollowKeys(leadState: LeadState): Iterable<FollowKey>;
    getFollowArgs(leadState: LeadState, key: FollowKey): Readonly<FollowArgs>;

    /*
    called when the lead (including all follows) is initialized
    */
    mapLeadSnapshot(
        leadState: LeadState,
        followStates: Record<FollowKey, FollowState>
    ): Iterable<E>;
    /*
    called when a follow query is initialized
    */
    mapFollowSnapshot(
        followKey: FollowKey,
        followState: FollowState,
        leadState: LeadState
    ): Iterable<E>;

    /*
    called for every event of the lead query
    */
    mapLeadEvent(
        leadEvent: LeadEvent,
        leadStateNext: LeadState, leadStatePrev: LeadState,
        followStates: Record<FollowKey, FollowState>
    ): Iterable<E>;
    /*
    called for every event of the follow query
    */
    mapFollowEvent(
        followEvent: FollowEvent,
        followKey: FollowKey,
        followStateNext: FollowState, followStatePrev: FollowState,
        leadState: LeadState,
    ): Iterable<E>;
}

export async function* createLeadFollowEvents<
    E, FollowKey extends string | number,
    LeadState, LeadEvent, LeadArgs extends Array<unknown>,
    FollowState, FollowEvent, FollowArgs extends Array<unknown>,
    >(
        config: LeadFollowEventsConfig<
            E, FollowKey,
            LeadState, LeadEvent, LeadArgs,
            FollowState, FollowEvent, FollowArgs
        >,
        args: LeadArgs,
): AsyncIterable<E> {
    const {
        signal,
        getFollowKeys, getFollowArgs,
        leadQueryFactory, followQueryFactory,
        mapLeadSnapshot, mapFollowSnapshot,
        mapLeadEvent, mapFollowEvent,
    } = config;

    const leadQueryPromise = leadQueryFactory.acquire(...args);
    const leadAbortController = new AbortController();
    /*
    when the signal is aborted, also abort the leadSignal
    */
    setupCascadingAbort(signal, leadAbortController);

    const followQueryPromises = {} as Record<
        FollowKey,
        Promise<QuerySource<FollowState, FollowEvent>>
    >;
    const followAbortControllers = {} as Record<
        FollowKey,
        AbortController
    >;

    try {

        //#region fn

        const initializeState = async () => {
            const followKeysToSpawn = Array.from(new Set(getFollowKeys(leadState)));
            for (const followKey of followKeysToSpawn) {
                const followArgs = getFollowArgs(leadState, followKey);
                followQueryPromises[followKey] = followQueryFactory.acquire(...followArgs);
            }
            await Promise.all(followKeysToSpawn.map(resolveFollowQuery));
        };

        const resolveFollowQuery = async (followKey: FollowKey) => {
            const followQueryPromise = followQueryPromises[followKey];
            const followAbortController = new AbortController();
            setupCascadingAbort(signal, followAbortController);

            const followQuery = await followQueryPromise;
            const followState = followQuery.getState();
            const followEventIterable = followQuery.fork(followAbortController.signal);

            assert(!(followKey in followAbortControllers));
            assert(!(followKey in followStates));
            assert(!(followKey in followEventIterables));

            followAbortControllers[followKey] = followAbortController;
            followStates = { ...followStates };
            followStates[followKey] = followState;
            followEventIterables[followKey] = followEventIterable;

            followAbortController.signal.addEventListener(
                "abort",
                () => {
                    assert(followKey in followAbortControllers);
                    assert(followKey in followStates);
                    assert(followKey in followEventIterables);

                    delete followAbortControllers[followKey];
                    followStates = { ...followStates };
                    delete followStates[followKey];
                    delete followEventIterables[followKey];
                },
            );

            return followQuery;
        };

        const emitSnapshotFromLeadQuery = () => {
            for (const event of mapLeadSnapshot(
                leadState,
                followStates,
            )) {
                sink.push(event);
            }
        };

        const emitSnapshotFromFollowQuery = (followKey: FollowKey) => {
            for (const event of mapFollowSnapshot(
                followKey,
                followStates[followKey],
                leadState,
            )) {
                sink.push(event);
            }
        };

        /*
        read lead query events and pass to sink, also end sink when this function ends
        */
        const emitEventsFromLeadQuery = async () => {
            for (const followKey of new Set(getFollowKeys(leadState))) {
                emitEventsFromFollowQuery(followKey).
                    catch(error => sink.error(error));
            }

            try {
                for await (
                    const { event: leadEvent, state: leadStateNext } of leadEventIterable
                ) {
                    const leadStatePrev = leadState;
                    leadState = leadStateNext;

                    for (const event of mapLeadEvent(
                        leadEvent,
                        leadStateNext, leadStatePrev,
                        followStates,
                    )) {
                        sink.push(event);
                    }

                    const followKeysPrev = new Set(getFollowKeys(leadStatePrev));
                    const followKeysNext = new Set(getFollowKeys(leadStateNext));

                    const followKeysToAbort = Array.from(followKeysPrev).
                        filter(followKeyPrev => !followKeysNext.has(followKeyPrev));
                    const followKeysToSpawn = Array.from(followKeysNext).
                        filter(followKeyNext => !followKeysPrev.has(followKeyNext));

                    for (const followKey of followKeysToAbort) {
                        assert(followKey in followAbortControllers);

                        followAbortControllers[followKey].abort();
                        followQueryFactory.release(followQueryPromises[followKey]);
                        delete followQueryPromises[followKey];
                    }
                    for (const followKey of followKeysToSpawn) {
                        assert(!(followKey in followQueryPromises));

                        const followArgs = getFollowArgs(leadStateNext, followKey);
                        followQueryPromises[followKey] =
                            followQueryFactory.acquire(...followArgs);
                    }
                    await Promise.all(followKeysToSpawn.map(resolveFollowQuery));

                    for (const followKey of followKeysToSpawn) {
                        emitSnapshotFromFollowQuery(followKey);
                    }

                    for (const followKey of followKeysToSpawn) {
                        emitEventsFromFollowQuery(followKey).
                            catch(error => sink.error(error));
                    }

                }
            }
            catch (error) {
                if (!leadAbortController.signal.aborted) throw error;
            }

            sink.done();
        };

        /*
        read follow query events and pass to sink
        */
        const emitEventsFromFollowQuery = async (followKey: FollowKey) => {
            try {
                for await (
                    const { event: followEvent, state: followStateNext } of
                    followEventIterables[followKey]
                ) {
                    const followStatePrev = followStates[followKey];
                    followStates = { ...followStates };
                    followStates[followKey] = followStateNext;

                    for (const event of mapFollowEvent(
                        followEvent,
                        followKey,
                        followStateNext, followStatePrev,
                        leadState,
                    )) {
                        sink.push(event);
                    }
                }
            }
            catch (error) {
                if (!followAbortControllers[followKey].signal.aborted) throw error;
            }

        };

        //#endregion

        /*
        setup everyting
        */
        const leadQuery = await leadQueryPromise;
        let leadState = leadQuery.getState();
        const leadEventIterable = leadQuery.fork(leadAbortController.signal);

        let followStates = {} as Record<
            FollowKey,
            FollowState
        >;
        const followEventIterables = {} as Record<
            FollowKey,
            AsyncIterable<QuerySourceElement<FollowState, FollowEvent>>
        >;

        /*
        We load the initial state
        */
        await initializeState();

        assertAbortSignal(signal, "lead-follow aborted");

        /*
        all events are emitted via sink
        */
        const sink = createBufferedAsyncIterable<E>();

        /*
         First we emit snapshots!
         */
        emitSnapshotFromLeadQuery();
        /*
        and then all the events
        */
        emitEventsFromLeadQuery().
            catch(error => sink.error(error));

        yield* sink;

        assertAbortSignal(signal, "lead-follow aborted");
    }
    finally {
        /*
        abort all follow queries
        */
        for (
            const followAbortController of
            Object.values<AbortController>(followAbortControllers)
        ) {
            followAbortController.abort();
        }
        /*
        release all follow queries
        */
        for (
            const followQueryPromise of
            Object.values<Promise<QuerySource<FollowState, FollowEvent>>>(followQueryPromises)
        ) {
            followQueryFactory.release(followQueryPromise);
        }

        /*
        abort lead query
        */
        leadAbortController.abort();
        /*
        release lead query query
        */
        leadQueryFactory.release(leadQueryPromise);
    }

}
