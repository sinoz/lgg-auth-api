import assert from "assert";

export function convertPgBinaryToBuffer(hex: string) {
    assert(hex.startsWith("\\x"), "expected \\x");

    const buffer = Buffer.from(hex.substring(2), "hex");
    return buffer;
}
