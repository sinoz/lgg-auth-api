import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("subscribe-client", t => withContext(async context => {
    const client = context.createApplicationClient();

    const subscribeClientResult = await client.subscribeClientEvents({
        parameters: {},
    });
    assert(subscribeClientResult.status === 200);

    const abortController = new AbortController();
    try {
        const eventIterable = subscribeClientResult.entities(abortController.signal);
        const eventIterator = eventIterable[Symbol.asyncIterator]();

        try {

            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type === "client-snapshot");
                t.deepEqual(
                    eventNext.value.payload.clients,
                    [
                        {
                            id: "MQ==",
                            name: "testing",
                        },
                    ],
                );
            }

            let clientId: string;
            {
                const result = await client.createClient({
                    parameters: {},
                    entity() { return { name: "testing-123" }; },
                });
                assert(result.status === 201);
                const entity = await result.entity();
                clientId = entity.id;
            }

            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type === "client-created");
                t.deepEqual(
                    eventNext.value.payload.client,
                    {
                        id: clientId,
                        name: "testing-123",
                    },
                );
            }
        }
        finally {
            abortController.abort();

            await eventIterator.next();
            t.fail("should throw");
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }

}));
