import * as authApi from "@latency.gg/lgg-auth-oas";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createDisconnectUserFromProviderSubjectOperation(
    context: application.Context,
): authApi.DisconnectUserFromProviderSubjectOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const { provider, subject, user } = incomingRequest.parameters;
        const userBuffer = Buffer.from(user, "base64");

        await withTransaction(
            context.services.pgPool,
            async pgClient => {

                const result = await pgClient.query(
                    `
delete from public.connection
where user_id = $1
and provider = $2
and subject = $3
;
`,
                    [
                        userBuffer,
                        provider,
                        subject,
                    ],
                );
            },
        );

        return {
            status: 204,
            parameters: {},
        };

    };
}
