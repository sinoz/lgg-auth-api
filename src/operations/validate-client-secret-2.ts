import * as authApi from "@latency.gg/lgg-auth-oas";
import * as crypto from "crypto";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createValidateClientSecret2Operation(
    context: application.Context,
): authApi.ValidateClientSecret2OperationHandler {
    return async function (incomingRequest) {
        const incomingEntity = await incomingRequest.entity();
        const clientIdBuffer = Buffer.from(incomingRequest.parameters.client, "base64");
        const clientSecretBuffer = Buffer.from(incomingEntity.secret, "base64");

        const valid = await withTransaction(
            context.services.pgPool,
            async pgClient => {
                let result;

                result = await pgClient.query(
                    `
select salt
from public.client
where id = $1
`,
                    [
                        clientIdBuffer,
                    ],
                );
                if (result.rowCount !== 1) {
                    return false;
                }

                const clientSaltBuffer = result.rows[0].salt as Buffer;

                const clientSecretHasher = crypto.createHash("sha512");
                clientSecretHasher.update(clientSecretBuffer);
                clientSecretHasher.update(clientSaltBuffer);
                const clientSecretDigest = clientSecretHasher.digest();

                result = await pgClient.query(
                    `
select 1
from public.client_secret
where client_id = $1
and digest = $2
`,
                    [
                        clientIdBuffer,
                        clientSecretDigest,
                    ],
                );
                if (result.rowCount !== 1) {
                    return false;
                }

                return true;
            },
        );

        return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    valid,
                };
            },
        };

    };
}
