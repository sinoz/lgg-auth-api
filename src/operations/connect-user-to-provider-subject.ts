import * as authApi from "@latency.gg/lgg-auth-oas";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createConnectUserToProviderSubjectOperation(
    context: application.Context,
): authApi.ConnectUserToProviderSubjectOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();
        const { provider, subject, user } = incomingRequest.parameters;
        const userBuffer = Buffer.from(user, "base64");

        await withTransaction(
            context.services.pgPool,
            async pgClient => {

                const result = await pgClient.query(
                    `
insert into public.connection(
    user_id, provider, subject, created_utc
)
values (
    $1, $2, $3, $4
)
;
`,
                    [
                        userBuffer,
                        provider,
                        subject,
                        now.toISOString(),
                    ],
                );
            },
        );

        return {
            status: 201,
            parameters: {},
        };

    };
}
