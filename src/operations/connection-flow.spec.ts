import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("authentication-flow", t => withContext(async context => {
    const client = context.createApplicationClient();

    let clientId: string;
    {
        const result = await client.createClient({
            parameters: {},
            entity() { return { name: "testing" }; },
        });
        assert(result.status === 201);
        const entity = await result.entity();
        clientId = entity.id;
    }

    let userId: string;
    {
        const result = await client.createUser({
            parameters: {
                client: clientId,
            },
            entity() {
                return {
                    name: "henkie",
                };
            },
        });
        assert(result.status === 201);
        const entity = await result.entity();
        userId = entity.id;
    }

    {
        const result = await client.connectUserToProviderSubject({
            parameters: {
                user: userId,
                provider: "google",
                subject: "123",
            },
            entity() {
                return {};
            },
        });
        assert(result.status === 201);
    }

    {
        const result = await client.resolveUserFromProviderSubject({
            parameters: {
                provider: "google",
                subject: "123",
            },
        });
        assert(result.status === 200);
        const entity = await result.entity();
        t.equal(entity.user, userId);
    }

    {
        const result = await client.disconnectUserFromProviderSubject({
            parameters: {
                user: userId,
                provider: "google",
                subject: "123",
            },
        });
        assert(result.status === 204);
    }

}));
