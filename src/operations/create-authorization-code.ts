import * as authApi from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import * as crypto from "crypto";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createAuthorizationCodeOperation(
    context: application.Context,
): authApi.CreateAuthorizationCodeOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();

        const expires = new Date(now.valueOf() + context.config.authorizationCodeAge);
        const clientId = incomingRequest.parameters.client;
        const clientIdBuffer = Buffer.from(clientId, "base64");
        const userId = incomingEntity.user;
        const userIdBuffer = Buffer.from(userId, "base64");

        const codeBuffer = await withTransaction(
            context.services.pgPool,
            async pgClient => {
                let result;

                /*
                find client row
                */
                result = await pgClient.query(
                    `
select id, salt
from public.client
where id = $1
;
`,
                    [
                        clientIdBuffer,
                    ],
                );

                assert(result.rowCount === 1);

                const [clientRow] = result.rows;

                const saltBuffer = clientRow.salt as Buffer;

                const codeBuffer = await new Promise<Buffer>(
                    (resolve, reject) => crypto.randomBytes(
                        512,
                        (error, value) => error ? reject(error) : resolve(value),
                    ),
                );

                const codeHasher = crypto.createHash("sha512");
                codeHasher.update(codeBuffer);
                codeHasher.update(saltBuffer);
                const codeDigestBuffer = codeHasher.digest();

                result = await pgClient.query(
                    `
insert into public.authorization_code(
    client_id, user_id,
    digest,
    created_utc, expires_utc
)
values (
    $1, $2,
    $3,
    $4, $5
)
returning *
;
`,
                    [
                        clientIdBuffer, userIdBuffer,
                        codeDigestBuffer,
                        now.toISOString(), expires.toISOString(),
                    ],
                );
                assert(result.rowCount === 1);

                return codeBuffer;
            },
        );

        return {
            status: 201,
            parameters: {},
            entity() {
                return {
                    code: codeBuffer.toString("base64"),
                    expires: expires.valueOf(),
                };
            },
        };
    };
}
