import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("client-credential-flow", t => withContext(async context => {
    const client = context.createApplicationClient();

    let clientId: string;
    let clientSecret: string;
    {
        const result = await client.createClient({
            parameters: {},
            entity() { return { name: "testing" }; },
        });
        assert(result.status === 201);
        const entity = await result.entity();
        clientId = entity.id;
    }

    {
        const result = await client.createClientSecret({
            parameters: {
                client: clientId,
            },
            entity() { return {}; },
        });
        assert(result.status === 201);
        const entity = await result.entity();
        clientSecret = entity.value;
    }

    {
        const result = await client.validateClientSecret2({
            parameters: {
                client: clientId,
            },
            entity() {
                return {
                    secret: clientSecret,
                };
            },
        });
        assert(result.status === 200);
        const entity = await result.entity();
        t.equal(entity.valid, true);
    }

    const redirectUri = "http://localhost:8080";
    {
        const result = await client.createRedirectUri({
            parameters: {
                client: clientId,
            },
            entity() {
                return {
                    value: redirectUri,
                };
            },
        });
        assert(result.status === 201);
    }

    {
        const result = await client.validateRedirectUri({
            parameters: {
                client: clientId,
            },
            entity() {
                return {
                    redirectUri,
                };
            },
        });
        assert(result.status === 200);
        const entity = await result.entity();
        t.equal(entity.valid, true);
    }

}));
