import * as authDb from "@latency.gg/lgg-auth-db";
import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { convertPgBinaryToBuffer, createRowQueryFactory, ErrorEvent, QuerySource, RowQueryState } from "../utils/index.js";

//#region query

export type ClientRowQuery = InstanceMemoizer<
    Promise<ClientRowQuerySource>, [Partial<authDb.ClientRow>]
>

export type ClientRowQuerySource = QuerySource<
    ClientRowState, ErrorEvent | authDb.ClientRowEvent
>

export function createClientRowQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "pgPool">,
    onError: (error: unknown) => void,
): ClientRowQuery {
    return createRowQueryFactory({
        pool: services.pgPool,
        getPrimaryKey: key => makePrimaryKey(key.id),
        getIndexKey: {},
        onError,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
        createTableEvents: authDb.createClientRowEvents,
    });

}

//#endregion

//#region state / events

export type ClientRowState = RowQueryState<authDb.ClientRow>;

//#endregion

//#region selectors

export function selectClientRow(
    state: ClientRowState,
    client: string,
): authDb.ClientRow | undefined {
    const key = makePrimaryKey(client);
    const row = state.rows.get(key);
    if (!row) return;

    return row;
}

export function createClientSnapshotEventFromRowState(
    state: ClientRowState,
): authSpec.ClientSnapshotSchema {
    const payload = {
        clients: state.rows.
            map(row => mapRowToEntity(row)).
            valueSeq().
            toArray(),
    };

    return {
        type: "client-snapshot",
        payload,
    };
}

export function createClientCreatedEventFromRowState(
    state: ClientRowState,
    client: string,
): authSpec.ClientCreatedSchema {
    const row = selectClientRow(state, client);
    assert(row != null, "expected row");

    const payload = {
        client: mapRowToEntity(row),
    };

    return {
        type: "client-created",
        payload,
    };
}

export function createClientUpdatedEventFromRowState(
    state: ClientRowState,
    client: string,
): authSpec.ClientUpdatedSchema {
    const row = selectClientRow(state, client);
    assert(row != null, "expected row");

    const payload = {
        client: mapRowToEntity(row),
    };

    return {
        type: "client-updated",
        payload,
    };
}

export function createClientDeletedEventFromRowState(
    state: ClientRowState,
    client: string,
): authSpec.ClientDeletedSchema {
    const row = selectClientRow(state, client);
    assert(row != null, "expected row");

    const payload = {
        client: mapRowToEntity(row),
    };

    return {
        type: "client-deleted",
        payload,
    };
}

//#endregion

//#region helpers

function makePrimaryKey(id: string) {
    return [id].join("\x1f");
}

function mapRowToEntity(
    row: authDb.ClientRow,
): authSpec.ClientEntitySchema {
    const { name } = row;
    const id = convertPgBinaryToBuffer(row.id).toString("base64");

    return {
        id, name,
    };
}

//#endregion
