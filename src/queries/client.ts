import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import deepEqual from "fast-deep-equal";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { createQueryFactory, ErrorEvent, isErrorEvent, QuerySource } from "../utils/index.js";
import * as clientRow from "./client-row.js";

//#region query

export type ClientQuery = InstanceMemoizer<
    Promise<ClientQuerySource>, []
>

export type ClientQuerySource = QuerySource<
    ClientState, ClientEventUnion | ErrorEvent
>

export function createClientQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "clientRowQuery">,
    onError: (error: unknown) => void,
): ClientQuery {
    return createQueryFactory({
        initialState, reduce,
        createSource,
        calculateHash: () => "",
        onError,
        settle: () => true,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
    ) {
        const queryPromise = services.clientRowQuery.acquire({});
        try {
            /**
             * wait for the query has sent it's first event, this will resolve immediately if
             * this query was already setup by an earlier require
             */
            const query = await queryPromise;

            let lastState = query.getState();

            /*
            Always emit a snaphot first
            */
            yield clientRow.createClientSnapshotEventFromRowState(lastState);

            /*
            then the rest of the events
            */
            for await (const { state, event } of query.fork(signal)) {
                if (isErrorEvent(event)) {
                    yield { error: true } as const;
                    break;
                }

                switch (event.type) {
                    case "snapshot": {
                        const nextEvent = clientRow.createClientSnapshotEventFromRowState(
                            state,
                        );
                        const prevEvent = clientRow.createClientSnapshotEventFromRowState(
                            lastState,
                        );

                        if (!deepEqual(nextEvent, prevEvent)) {
                            yield nextEvent;
                        }

                        break;
                    }

                    case "insert": {
                        const client = event.key.id;
                        const nextEvent = clientRow.createClientCreatedEventFromRowState(
                            state, client,
                        );

                        yield nextEvent;
                        break;
                    }

                    case "update": {
                        const client = event.key.id;
                        const nextEvent = clientRow.createClientUpdatedEventFromRowState(
                            state, client,
                        );

                        yield nextEvent;
                        break;
                    }

                    case "delete": {
                        const client = event.key.id;
                        const nextEvent = clientRow.createClientDeletedEventFromRowState(
                            lastState, client,
                        );

                        yield nextEvent;
                        break;
                    }
                }

                lastState = state;
            }
        }
        finally {
            services.clientRowQuery.release(queryPromise);
        }
    }
}

//#endregion

//#region state / events

export interface ClientEntity {
    name: string
}

export interface ClientState {
    error: boolean;
    entities: immutable.Map<string, ClientEntity>;
}

const initialState: ClientState = {
    error: false,
    entities: immutable.Map<string, ClientEntity>(),
};

export type ClientEventUnion =
    authSpec.ClientSnapshotSchema |
    authSpec.ClientCreatedSchema |
    authSpec.ClientDeletedSchema |
    authSpec.ClientUpdatedSchema;

function reduce(
    state: ClientState,
    event: ErrorEvent | ClientEventUnion,
): ClientState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "client-snapshot": {
            let { entities } = initialState;
            entities = entities.asMutable();

            for (const { id, name } of event.payload.clients) {
                assert(!entities.has(id), "client already exist");

                const entity = {
                    name,
                };
                entities.set(id, entity);
            }

            entities.asImmutable();

            return {
                error: false,
                entities,
            };
        }

        case "client-created": {
            let { entities } = state;

            const { id, name } = event.payload.client;

            assert(!entities.has(id), "client already exist");

            const entity: ClientEntity = {
                name,
            };
            entities = entities.set(id, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-updated": {
            let { entities } = state;

            const { id, name } = event.payload.client;

            let entity = entities.get(id);
            assert(entity, "client does not exist");
            entity = {
                ...entity,
                name,
            };

            entities = entities.set(id, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-deleted": {
            let { entities } = state;

            const { id } = event.payload.client;

            assert(entities.has(id), "client does not exist");
            entities = entities.delete(id);

            return {
                ...state,
                entities,
            };
        }

        default: return state;
    }
}

//#endregion

//#region selectors

export function selectClientSnapshotEvent(
    state: ClientState,
): authSpec.ClientSnapshotSchema {
    const payload = {
        clients: state.entities.
            map(({ name }, id) => ({
                id,
                name,
            })).
            valueSeq().
            toArray(),
    };

    return {
        type: "client-snapshot",
        payload,
    };
}

export function selectClientExists(
    state: ClientState,
    client: string,
): boolean {
    return state.entities.has(client);
}

export function selectClientEntity(
    state: ClientState,
    client: string,
): ClientEntity | undefined {
    const clients = state.entities;
    const entity = clients.get(client);
    if (!entity) return;

    return entity;
}

//#endregion
